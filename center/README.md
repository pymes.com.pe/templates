# Center
pk:16
name:Center
dir_name:center
price:0
private:0
slider:0
css:pymes/center-nc.min.css
js:-
prefix:pymes
status:0
## Fonts
Roboto 400
#0#a61530
#0#ec407a
#0#c1283d
#0#673bb7
#0#3f51b5
#0#02203f
#0#1a77d4
#0#043669
#0#18e2ba
#0#07aaf5
#0#1663b0
#0#27a79a
#0#0E8044
#0#77be32
#0#00b55f
#0#8dc24c
#0#07cb79
#0#02ab68
#0#ff7221
#0#bf8d5c
#0#ff9801
#0#ab987a
#0#000000
#0#ef7c46