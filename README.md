# Templates
Templates for our platform
Clonar el repositorio
requerido: Registrar las plantillas en el sistema
git clone https://gitlab.com/pymes.com.pe/templates.git

## Compile with scss##
```sh
npm run watch-css
scss style.scss style.css --style compressed
gzip -9 style.css
mv style.css.gz style.css
```

## Upload to S3-Amazon ##
Configuration for upload to S3 Amazon
```sh
storage class: Use Reduced Redundancy Storage: Set true
Everyone: Open/Download - Read
Expires: Thu, 31 Dec 2099 20:00:00 GMT
Cache-Control: max-age=94608000
Content-Encoding: gzip
```

## Required
```sh
apt-get install ruby-full
gem install sass
```
## Libs
- `bulma.min.css`: Contain bulma.min v0.6 and fontawesome

# VARS #
For build a template you have vars:

## web
web site informatión use var like `web.var_name`

- `name`: Site Name

## page
Page page.var_name, each language have one page

* `address`: Web site address
* [menu]: Web site address


### Menu
- `id`
- `title`
- `url`
- `expanded`
- `submenu`
- `num_elements`

```php
{{menu.title}}
```
