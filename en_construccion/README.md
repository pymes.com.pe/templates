# Pagina en construcción
pk:2
name:Construction
dir_name:en_construccion
price:0
private:0
slider:0
css:pymes/en_construccion-nc.min.css
js:-
prefix:pymes
status:0
## LIBS
http://hilios.github.io/jQuery.countdown/examples.html

# Basic Reloj

Copiar y pegar a JS

```javascript
$("#basic").countdown('2020/10/10', function(event) {
var $this = $(this).html(event.strftime(''
+ '%w<strong>S</strong>'
+ '%d<strong>D</strong>'
+ '%H<strong>H</strong>'
+ '%M<strong>M</strong>'
+ '%S<strong>S</strong>'));
});

  $("#advance").countdown('2018/12/30 12:00:00').on('update.countdown', function(event) {
var $this = $(this).html(event.strftime(''
+ '<div class="counter-container"><div class="counter-box first"><div class="number">%-D</div><span>Day%!d</span></div>'
+ '<div class="counter-box"><div class="number">%H</div><span>Hours</span></div>'
+ '<div class="counter-box"><div class="number">%M</div><span>Minutes</span></div>'
+ '<div class="counter-box last"><div class="number">%S</div><span>Seconds</span></div></div>'
));
});
```

## Colors
#0#a61530
#0#ec407a
#0#c1283d
#0#673bb7
#0#3f51b5
#0#02203f
#0#1a77d4
#0#043669
#0#18e2ba
#0#07aaf5
#0#1663b0
#0#27a79a
#0#0E8044
#0#77be32
#0#00b55f
#0#8dc24c
#0#07cb79
#0#02ab68
#0#ff7221
#0#bf8d5c
#0#ff9801
#0#ab987a
#0#000000
#0#ef7c46